import smartpy as sp

class Looping(sp.Contract):
    def __init__(self):
        self.init(sum = 0, sum1 = 0)
        
    @sp.entryPoint
    def whileLoop(self, params):
            i = sp.newLocal("i", 0)
            sp.while i < params.n:
              self.data.sum += 1
              i.set(i + 2)
           
    @sp.entryPoint
    def forLooping(self,params):
            i = sp.newLocal("i", 0)
            sp.for i in sp.range(1, 6):
                self.data.sum1 += 1
    
@addTest(name = "Test")
def test():
    scenario = sp.testScenario()
    scenario.h1("Loop")
    
    c1 = Looping()
    scenario += c1

    scenario.h2("While Loop")
    scenario += c1.whileLoop(n=5)
    
    scenario.h2("For Loop")
    scenario += c1.forLooping()
