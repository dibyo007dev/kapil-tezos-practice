import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self):
        self.init(cparty= sp.address("tz1W5rFBzbj6Ky7n6ALMaQdMWwBRHPRiQD87"))

    @sp.entryPoint
    def buyMango(self,params):
        sp.if sp.amount > sp.tez(200000):
            sp.send(self.data.cparty,sp.tez(200000))

    @sp.entryPoint
    def buyApple(self,params):
        sp.if sp.amount > sp.tez(15000000):
            sp.send(self.data.cparty,sp.tez(15000000))
    
    @sp.entryPoint
    def buyGuava(self,params):
        sp.if sp.amount > sp.tez(10000000):
            sp.send(self.data.cparty,sp.tez(10000000))
        
@addTest(name="test")
def test():
    obj = MyContract()
    scenario = sp.testScenario()
    scenario += obj
    scenario += obj.buyMango().run(sender = "tz1-AAA",amount=sp.tez(10))

