#!/usr/bin/env node

var program = require('commander');

program
  .version('0.0.1')
  .command('add <notes>')
  .alias('a')
  .description('Add a new note')
  .action(note=>{
  	console.log(note)
  })
  .option('-d, --debug', 'output extra debugging')
  .option('-t, --type', 'software type')
  .option('-p, --port <port>', 'use custom port')
  .option('-f, --flag', 'boolean flag', false)
  .parse(process.argv)

console.log(process.argv);
