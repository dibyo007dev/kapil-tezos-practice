const conseiljs = require('conseiljs');
const tezosNode = 'https://rpcalpha.tzbeta.net';

conseiljs.setLogLevel('debug');

async function deployContract() {
    const keystore = {
        publicKey: 'edpkv9b4orJp17kYqWZvu12S8HFDtjmV6MnDBqQp8cRtgKqyMQV65V',
        privateKey: 'edskRd6F8SjcQ6ZwNGf2hWZGiVjwE1w9Y3mkVxGZxZAeXXHGMYxCRNgtyJiSZPgCnRtM7VPn8DPgUuGDtXuorYqGj6PXVSwgPk',
        publicKeyHash: 'tz1PQBDG5QxhvaE9zvdr1i4xyn76pAhsmJeV',
        seed: '',
        storeType: conseiljs.StoreType.Fundraiser
    };

     const contract = `[
                        {"prim":"storage","args":[
                         {"prim":"int"}]
                        },
                        {"prim":"parameter","args":[
                         {"prim":"pair","args":[
                          {"prim":"int","annots":["%addend"]},{"prim":"int","annots":["%augend"]}]
                         }]
                        },
                        {"prim":"code","args":[
                         [{"prim":"DUP"},{"prim":"CDR"},{"prim":"SWAP"},{"prim":"CAR"},{"prim":"DUP"},{"prim":"CAR"},{"prim":"SWAP"},{"prim":"DUP"},{"prim":"DUG","args":[{"int":"2"}]},{"prim":"CDR"},{"prim":"ADD"},{"prim":"DUG","args":[{"int":"2"}]},{"prim":"DROP"},{"prim":"DROP"},
                         {"prim":"NIL","args":[
                          {"prim":"operation"}]
                         },{"prim":"PAIR"}]]
                        }]`;

    const storage = '{"string": "Sample"}';  
    
    const result = await conseiljs.TezosNodeWriter.sendContractOriginationOperation(tezosNode, keystore, 0, undefined, false, true, 100000, '', 1000, 100000, 
                                                                                    contract, storage, conseiljs.TezosParameterFormat.Micheline);
    
    console.log(`Injected operation group id ${result.operationGroupID}`);

}

deployContract();

